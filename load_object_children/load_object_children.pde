
PShape s;
PShape[] children;
int childCount;


float rot = 0.0;
float velocity = 0.001;


float ry;

int startFaceIndex = 0;
int stopFaceIndex;

int maxFaceIndex;

void setup() {
  //size(800, 800, P3D);
  fullScreen(P3D);
  // The file "bot.obj" must be in the data folder
  // of the current sketch to load successfully
  s = loadShape("Echinacea Julia - Coneflower.obj");

  println("childCount", childCount);

  maxFaceIndex = s.getChildCount();
  stopFaceIndex = maxFaceIndex;

  //s = new OBJModel(this, "BurkaMan_Alone2.obj", "relative", QUADS);
}

void draw() {

  background(193, 184, 172);
  noCursor();

  translate(width/2, height/1.7);
  rotateX(65);
  //rotateX(map(mouseY, 0, height, -PI, PI));

  //rotateZ(rot);
  rotateY(-rot);
  //rotateZ(rot);
  scale(17);

  //stroke(0);
  //s.shapeMode(TRIANGLES);
  //shape(s, 0, 0);
  drawFaceSelection();

  rot+= velocity;
}

void drawFaceSelection() 
{

  lights();
  ambientLight(209, 0, 156, 1, 1, 1);
  directionalLight(255, 266, 266, -1, 0, 1);

  //directionalLight(40, 189, 255, 1, 0, 1);




  beginShape(QUADS);

  for (int i = startFaceIndex; i < stopFaceIndex; i++) {
    PShape face = s.getChild(i);
    int numVertices = face.getVertexCount();


    for (int j = 0; j < numVertices; j++) {
      strokeWeight(0.025);
      stroke(0, 100);
      float bruitX = 0.00004;
      float bruit_x = noise(millis() * bruitX) * 40; 
      fill(0, 255, 100, 200);
      vertex(face.getVertexX(j), face.getVertexY(j), face.getVertexZ(j));
    }
  }

  endShape(CLOSE);
}

void mouseDragged() {
  if (keyPressed) {
    startFaceIndex = (int)map(mouseX, 0, width, 0, maxFaceIndex-1);
    startFaceIndex = constrain(startFaceIndex, 0, maxFaceIndex-1);
  } else {
    stopFaceIndex = (int)map(mouseX, 0, width, 1, maxFaceIndex-1);
    stopFaceIndex = constrain(stopFaceIndex, 0, maxFaceIndex-1);
  }
}
