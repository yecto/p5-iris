
int num = 50;
float[] x = new float[num];
float[] y = new float[num];

void setup() {
  size(400, 400);
  noStroke();
  smooth();
  fill(255, 102);
}

void draw() {
  background(0);
  toyollo();
}

void toyollo() {
  float ruidoX = 0.0009;
  float posX = noise(millis() * ruidoX) * width;

  float ruidoY = 0.001;
  float posY = noise(millis() * ruidoY) * height;

  for (int i = num-1; i > 0; i--) {
    x[i] = x [i-1];
    y[i] = y [i-1];
  }
  x[0] = posX;
  y[0] = posY;
  for (int i = 0; i < num; i++) {
    ellipse(x[i], y[i], i/4, i/4);
    // ellipse(posX, posY, i/4, i/4);
  }
}
