/////////////////////////// INITIALISE GEOMETRY /////////////////

void createMesh() {

  HEC_Cylinder creator=new HEC_Cylinder();

  //CREATOR PARMAMETERS
  creator.setFacets(20); 
  creator.setHeight(60).setRadius(60); // 
  creator.setSteps(40).setCap(false, false); // Vertical divisions & taper
  // creator.setTaper(1.7);

  MESH = new HE_Mesh(creator);  // ADD OUR CREATOR PARAMETERS TO OUR MESH
}


void createModifiers() {

  // BEND MODIFIER
  // Twist Modifier
  HEM_Twist twist = new HEM_Twist().setAngleFactor(240+ ruido);
  L = new WB_Line(1, 0, 1, -1, 0, -1);
  twist.setTwistAxis(L);
  MESH.modify(twist);

  // INTERSTING MODIFIER
  MESH.modify( new HEM_Soapfilm().setIterations(10)); 
  HET_Diagnosis.validate( MESH );

  RENDER = new WB_Render(this); // RENDER MESH
}

/////////////////////////// INITIALISE GEOMETRY /////////////////

void createMesh1() {

  HEC_Cylinder creator=new HEC_Cylinder();

  //CREATOR PARMAMETERS
  creator.setFacets(120); 
  creator.setHeight(40).setRadius(40); // 
  creator.setSteps(4).setCap(false, false); // Vertical divisions & taper
  // creator.setTaper(1.7);

  MESH1 = new HE_Mesh(creator);  // ADD OUR CREATOR PARAMETERS TO OUR MESH
}


void createModifiers1() {

  // BEND MODIFIER
  // Twist Modifier
  HEM_Twist twist = new HEM_Twist().setAngleFactor(5 + ruido);
  L = new WB_Line(4 + ruido, 0, 4, -4, 0, -4);
  twist.setTwistAxis(L);
  MESH1.modify(twist);

  // INTERSTING MODIFIER
  MESH1.modify( new HEM_Soapfilm().setIterations(8)); 
  HET_Diagnosis.validate( MESH1 );

  RENDER1 = new WB_Render(this); // RENDER MESH
}
