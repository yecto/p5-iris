import oscP5.*;
import netP5.*;
import oscP5.*;
import netP5.*;
import processing.pdf.*;

OscP5 osc;

import ddf.minim.*;
Minim minim;
AudioInput in;

// he_mesh

import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;
import processing.opengl.*;

// HEMESH CLASSES & OBJECTS
HE_Mesh MESH; // Our mesh object
WB_Render RENDER; // Our render object

HE_Mesh MESH1; // Our mesh object
WB_Render RENDER1; // Our render object

// Check these two objects
//WB_Plane P; 
WB_Line L;

import peasy.*;
PeasyCam CAM;
int FRAME = 0;
//

// variables

float ruido;
int z;
int za = 2;
boolean record;
PFont f;
String words = "a";
int ml = words.length();
int palabras = 0;
int girar = 90;
float oll = 230;
float mapa = 1;
float momento = 30;
int t = 6;
int diez;
int dato;


int numberFrames = 1000, step=10, border=100, dmax=80;
float theta;

int contador;

void setup() {
  smooth();
  noCursor();
  fullScreen(OPENGL);
  printArray(PFont.list());
  colorMode(RGB);
  CAM = new PeasyCam(this, 200);
  osc = new OscP5(this, 8010);
  minim = new Minim(this);
  minim.debugOn();
  in = minim.getLineIn(Minim.STEREO, 1024);
}

void draw() {

  createMesh();
  createModifiers();
  //

  createMesh1();
  createModifiers1();

  background(0);

  for (int i = 0; i < in.bufferSize() -1; i++) {
    ruido = (ruido + in.right.get(i)*40)*sin(radians((in.bufferSize()/momento)*i));
  }

  directionalLight(255, 130, 7, 1, 0, -1);
  directionalLight(255, 255, 255, -1, 0, 1);
  directionalLight(0, 255, 255, 0, 1, -1);
  directionalLight(255, 40, 145, -1, 0, 1);


  vez();

  theta -= TWO_PI/numberFrames;
  //if (frameCount<=numberFrames) saveFrame("image-####.tif");
}

void keyPressed() {

  if (key == 's') {
    saveFrame("screenShot_###.png");
    println("screen shot taken");
  }
  if (key == 'o') {
    // reset camera origin positions  - do this before
    // exporting your shape so your shape is positioned
    // on a flat plane ready for 3D printing
    CAM.reset(1000);
  }
  // Print camera position - could be helpful
  if (key == 'p') {
    float[] camPos = CAM.getPosition();
    println(camPos);
  }
}


void stop() {
  in.close();
  minim.stop();
  super.stop();
}
