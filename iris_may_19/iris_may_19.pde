//This demo allows wekinator to control x, y, size, hue, and rotation of an object
//All are continuous values between 0 and 1

//Necessary for OSC communication with Wekinator:
import oscP5.*;
import netP5.*;
OscP5 oscP5;
NetAddress dest;

// Variables for object
PShape s;
PShape[] children;
int childCount;
float rot = 0.0;
float velocity = 0.01;
//Parameters of sketch
float myX, myY, mySize, myHue, myRot, myScale;
PFont myFont;

int maxFaceIndex;
int startFaceIndex = 0;
int stopFaceIndex;


void setup() {
  //Initialize OSC communication
  oscP5 = new OscP5(this, 12000); //listen for OSC messages on port 12000 (Wekinator default)
  dest = new NetAddress("127.0.0.1", 6448); //send messages back to Wekinator on port 6448, localhost (this machine) (default)
  //s = loadShape("BurkaMan_Alone2.obj");
  s = loadShape("Echinacea Julia - Coneflower.obj");

  println("childCount", childCount);
  maxFaceIndex = s.getChildCount();
  stopFaceIndex = maxFaceIndex;

  colorMode(HSB);
  size(400, 400, P3D);
  smooth();
  background(255);

  //Initialize appearance
  myX = 200;
  myY = 200;
  mySize = 400;
  myHue = 255;
  myRot = 40;
  myScale = 2;
  sendOscNames();
  myFont = createFont("Arial", 14);
}

void draw() {
  background(100);

  pushMatrix();

  noStroke();
  lights();

  translate(myX+mySize/2, myY+mySize/2);
  fill(color(myHue, myHue, myHue));
  rotate(myRot);
  rotateY(rot);
  scale(myScale);
  drawFaceSelection();

  popMatrix();
  rot += velocity;

  drawtext();
}




//This is called automatically when OSC message is received
void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/wek/outputs")==true) {
    if (theOscMessage.checkTypetag("fffffff")) { // looking for 5 parameters
      float receivedX = theOscMessage.get(0).floatValue();
      float receivedY = theOscMessage.get(1).floatValue();
      float receivedSize = theOscMessage.get(2).floatValue();
      float receivedHue = theOscMessage.get(3).floatValue();
      float receivedRot = theOscMessage.get(4).floatValue();
      float receivedScale = theOscMessage.get(5).floatValue();
      float receivedMouseX = theOscMessage.get(6).floatValue();


      myX = map(receivedX, 0, 1, -mySize/2, width-mySize/2);
      myY = map(receivedY, 0, 1, -mySize/2, height-mySize/2);
      mySize = map(receivedSize, 0, 1, 0, 200);
      myHue = map(receivedHue, 0, 1, 0, 255);
      myRot = map(receivedRot, 0, 1, 0, TWO_PI);
      myScale = map(receivedScale, 0, 1, 0, 12);

      // trying to implement mesh appearing-dissappearing

      startFaceIndex = (int)map(receivedMouseX, 0, width, 0, maxFaceIndex-1);
      startFaceIndex = constrain(startFaceIndex, 0, maxFaceIndex-1);


      // println("Received new output values from Wekinator");
    } else {
      stopFaceIndex = (int)map(30, 0, width, 1, maxFaceIndex-1);
      stopFaceIndex = constrain(stopFaceIndex, 0, maxFaceIndex-1);      
      println("Error: unexpected OSC message received by Processing: ");
      theOscMessage.print();
    }
  }
}

//Sends current parameter (hue) to Wekinator
void sendOscNames() {
  OscMessage msg = new OscMessage("/wekinator/control/setOutputNames");
  msg.add("X"); //Now send all 5 names
  msg.add("Y");
  msg.add("Size");
  msg.add("Hue");
  msg.add("Rotation");
  oscP5.send(msg, dest);
}

//Write instructions to screen.
void drawtext() {
  stroke(0);
  textFont(myFont);
  textAlign(LEFT, TOP);
  fill(0, 0, 255);

  text("Listening for message /wek/inputs on port 12000", 10, 10);
  text("Expecting 5 continuous numeric outputs, all in range 0 to 1:", 10, 25);
}
