void drawFaceSelection() 
{


  beginShape(QUADS);

  for (int i = startFaceIndex; i < stopFaceIndex; i++) {
    PShape face = s.getChild(i);
    int numVertices = face.getVertexCount();


    for (int j = 0; j < numVertices; j++) {
      strokeWeight(0.25);
      stroke(0);
      vertex(face.getVertexX(j), face.getVertexY(j), face.getVertexZ(j));
    }
  }

  endShape(CLOSE);
}
